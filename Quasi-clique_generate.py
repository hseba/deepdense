from networkx.generators import community
from networkx.generators import random_graphs
from networkx.algorithms import clique
import networkx as nx
import random,numpy as np,os
import copy
import matplotlib.pyplot as plt

def generate_clique(nb,size,total_size):
    sub = community.caveman_graph(nb, size)
    G = random_graphs.fast_gnp_random_graph(total_size, 0.07)  # Generation de graphes aleatoires avec 0.1% de liens
    G = nx.compose(G, sub)  # fusion des deux graphes, obtention d'un graphe aleatoire avec nb cliques
    node_mapping = dict(zip(G.nodes(), sorted(G.nodes(), key=lambda k: random.random())))  # creation du mapping
    G = nx.relabel_nodes(G, node_mapping)
    cliques = list(clique.find_cliques(G))
    cliquess = np.asarray(([y for x in cliques for y in x if len(x) >= 4]))
    for i in range(100):
        if i not in cliquess:
            x = list(nx.neighbors(G, i))
            for k in cliques:
                v = 0
                for j in range(len(x)):
                    if x[j] in k and len(k) > 3:
                        v += 1

                        if v >= len(k) - 2:
                            cliquess = np.append(cliquess, [i])
                            break
    out = np.zeros(100)
    out[cliquess] = 1

    return G,out,len(np.unique(cliquess)),cliquess,size



def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="Quasi_clique"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_size_clique = 20
max_clique_count = 5
outputs = []
Gr_size = 1000
graph = []
data = []
lab = []
nodes = []
input = []
sz = []
B = [None]*total_size
x = 0
for id in range(Gr_size):
    G,labels,y,z,s = generate_clique(random.randint(4,max_clique_count),random.randint(4,max_size_clique),total_size)
    tab = to_input_shape(G)
    graph.append(tab)
    A = nx.adjacency_matrix(G, nodelist=range(total_size), weight='weight')
    A.setdiag(A.diagonal() * 2)
    A = A.todense()
    print("la taille : ",s)
    B = copy.deepcopy(A)
    input.append(A)
    for i in range(len(B)):
        if i not in z:
            B[i] = 0
    outputs.append(y)
    lab.append(labels)
    data.append(B)
    sz.append(s)
    print(id)
print("graphe",graph[0])
print("matrix",data[0])

np.save(os.path.join(PATH, "size.npy"), np.asarray(sz))
np.save(os.path.join(PATH, "data.npy"), np.asarray(graph))
np.save(os.path.join(PATH, "data2.npy"), np.asarray(data))
output = np.asarray(outputs)
np.save(os.path.join(PATH,"output.npy"),output)
print("out",output[0])
labs = np.asarray(lab)
np.save(os.path.join(PATH,"labels2.npy"),labs)
print("labs",labs[0])
node = np.asarray(input)
np.save(os.path.join(PATH,"nodes.npy"),node)
print("nodes",node[0])

