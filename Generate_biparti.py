
import networkx as nx
import matplotlib.pyplot as plt
from networkx.generators import random_graphs
import random,numpy as np,os
import copy

def generate_clique(nb,size,total_size):
    sub = nx.complete_bipartite_graph(nb, size)
    G=random_graphs.fast_gnp_random_graph(total_size,0.01)#Generation de graphes aleatoires avec 0.1% de liens
    GS=nx.compose(G,sub) #fusion des deux graphes, obtention d'un graphe aleatoire avec nb cliques
    node_mapping = dict(zip(GS.nodes(), sorted(GS.nodes(), key=lambda k: random.random())))#creation du mapping
    G_new = nx.relabel_nodes(GS, node_mapping)#application du mapping
    A = nx.adjacency_matrix(G_new,nodelist=sorted(G.nodes()),weight='weight')
    A.setdiag(A.diagonal() * 2)
    A = A.todense()
    for i in range(len(A)):
        if (np.count_nonzero(A[i] == 1) > 4):
            Bipartie.append(i)
    output=np.zeros(total_size)
    output[Bipartie]=1
    return G_new,output,len(Bipartie),Bipartie,(size+nb),A

def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="Bipartie"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_size_clique = 30
max_clique_count = 30
outputs = []
Gr_size = 1
graph = []
data = []
lab = []
nodes = []
input = []
sz = []
B = [None]*total_size
x = 0
for id in range(Gr_size):
    Bipartie = []
    G,labels,y,z,s,A = generate_clique(random.randint(5,max_clique_count),random.randint(5,max_size_clique),total_size)
    tab = to_input_shape(G)
    graph.append(tab)
    B = copy.deepcopy(A)
    input.append(A)
    for i in range(len(B)):
        if i not in z:
            B[i] = 0
    outputs.append(y)
    lab.append(labels)
    data.append(B)
    T = nx.edges(G)
    T = np.asarray(T)
    E = T

    for i in range(len(E)):
        x = E[i, 0]
        c = E[i, 1]
        if (x not in z) and (c not in z):
            w = -1
            t = np.argwhere(T == (x, c))
            d = np.argwhere(T == (c, x))
            t = np.concatenate((t, d))
            for r in range(len(t)):
                for k in range(len(t)):
                    if (t[r, 0] == t[k, 0]) and r != k and w != t[r, 0]:
                        w = t[r, 0]
                        print("w", w)
            P = np.delete(T, w, axis=0)
            print(len(P), E[i])
            T = P

    sz.append(T)


output = np.asarray(outputs)
labs = np.asarray(lab)
node = np.asarray(input)


nx.draw(G, with_labels=True)
plt.show()
np.save(os.path.join(PATH, "size.npy"), np.asarray(sz[0])) ###########################
np.save(os.path.join(PATH, "data.npy"), np.asarray(graph)) ############################
np.save(os.path.join(PATH, "data2.npy"), np.asarray(data)) ##########################
np.save(os.path.join(PATH,"output.npy"),output) #generation des outputs #######################
np.save(os.path.join(PATH,"labels2.npy"),labs) #generation des outputs ##########################
np.save(os.path.join(PATH,"nodes.npy"),node) #generation des outputs
