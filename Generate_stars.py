from networkx.generators import community
from networkx.generators import random_graphs
from networkx.algorithms import clique
import networkx as nx
import random,numpy as np,os
import copy
import matplotlib.pyplot as plt

def generate_clique(nb,size,total_size):
    j = 0
    sub=nx.Graph()#Generation de de graphes en forme de cliques
    for i in range(nb):
        nx.add_star(sub,[j,j+1,j+2,j+3,j+4,j+5])
        j = j + 6
    #nx.draw(sub)
    #plt.show()
    G=random_graphs.fast_gnp_random_graph(total_size,0.000000001)#Generation de graphes aleatoires avec 0.1% de liens
    G=nx.compose(G,sub) #fusion des deux graphes, obtention d'un graphe aleatoire avec nb cliques
    #nx.draw(G)
    #plt.show()
    node_mapping = dict(zip(G.nodes(), sorted(G.nodes(), key=lambda k: random.random())))#creation du mapping
    G_new = nx.relabel_nodes(G, node_mapping)#application du mapping
    A = nx.adjacency_matrix(G_new, nodelist=range(total_size), weight='weight')
    A.setdiag(A.diagonal() * 2)
    A = A.todense()
    B = copy.deepcopy(A)
    output=np.zeros(total_size)
    sortie=np.zeros(total_size)
    k = []
    for i in range(len(B)):
        if (np.count_nonzero(A[i] == 1) < 5):
            B[i] = 0
        else:
            sortie[i] = 1
            k.append(i)
            for j in range(len(B)):
                if B[i,j] == 1:
                    sortie[j] = 1
                    k.append(j)
    print("k",len(k),k)
    return G_new,sortie,4,B,5,A,k

def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="star"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_star_clique = 20
max_star_count = 12
outputs = []
Gr_size = 100
graph = []
data = []
lab = []
nodes = []
input = []
sz = []
x = 0
for id in range(Gr_size):
    G,labels,y,B,s,A,o = generate_clique(random.randint(4,max_star_count),random.randint(4,max_star_clique),total_size)
    #G,labels,y,z,s = generate_clique(,4,total_size)
    tab = to_input_shape(G)
    graph.append(tab)
    outputs.append(y)
    lab.append(labels)
    input.append(A)
    data.append(B)
    T = nx.edges(G)
    T = np.asarray(T)
    print("hay len ya t7a7na",len(T),T)
    E = T
    print("hay len ya t7a7na",len(T))

    for i in range(len(E)):
        x = E[i,0]
        c = E[i,1]
        if (x not in o) and (c not in o):
            w = -1
            t = np.argwhere(T == (x, c))
            d = np.argwhere(T == (c, x))
            t = np.concatenate((t, d))
            print("madkhelch")
            for r in range(len(t)):
                for k in range(len(t)):
                    if (t[r, 0] == t[k, 0]) and r != k and w != t[r, 0]:
                        w = t[r, 0]
                        print("w",w)
            P = np.delete(T,w,axis=0)
            print(len(P),E[i])
            T=P
    print("hay len ya t7a7na",len(T))

    sz.append(T)
    print(T)
    print(y)
    print(id)


print("graphe",len(sz[0]),len(sz))
print("matrix",np.count_nonzero(data[0]==1))
np.save(os.path.join(PATH, "size.npy"), np.asarray(sz[0])) ###########################
np.save(os.path.join(PATH, "data.npy"), np.asarray(graph)) ############################
np.save(os.path.join(PATH, "data2.npy"), np.asarray(data)) ##########################
output = np.asarray(outputs)
np.save(os.path.join(PATH,"output.npy"),output) #generation des outputs #######################
print("out",output[0])
labs = np.asarray(lab)
np.save(os.path.join(PATH,"labels2.npy"),labs) #generation des outputs ##########################
print("labs",np.count_nonzero(labs[0]==1))
node = np.asarray(input)
np.save(os.path.join(PATH,"nodes.npy"),node) #generation des outputs
print("nodes",np.count_nonzero(node[0]==1))