import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time
from multiprocessing import Pool
from itertools import repeat
from ANN_CLIQUES import PatternFinding
from train import Training

def localisation(G,i):
    X = []
    a = list(G.neighbors(i))
    if len(G.edges(i)) > 10:
            for j in sorted(G.nodes()):
                """
                if j > i and G.number_of_edges(i, j) == 0:
                    X.append(j)
                else:
                    Y.append(j)
                """
                if j > i and G.number_of_edges(i,j) == 0 and len(G.edges(j)) > 10:
                    V = []
                    V.append(i)
                    V.append(j)

                    b = list(G.neighbors(j))
                    c = list(set(a).intersection(b))
                    t = sorted(c)
                    if len(c) > 0 and t[0] > V[0]:
                        V = V + c
                    """

                    for k in c:
                        if i != j and G.number_of_edges(i, j) == 0 and G.number_of_edges(i,k) == 1 and G.number_of_edges(j, k) == 1:
                            V.append(k)
                        else:
                            if i != j and G.number_of_edges(i, j) == 1 and G.number_of_edges(i,k) == 0 and k in G.nodes() and G.number_of_edges(j, k) == 0 and Voisin(k, V, G):
                                V.append(k)

                    #print("le commun entre : ",i,j,list(set(a) & set(b)))
                    for k in G.nodes():
                        if i != j and G.number_of_edges(i,j) == 0 and G.number_of_edges(i,k) == 1 and G.number_of_edges(j,k) == 1:
                            V.append(k)
                        else:
                            if i != j and G.number_of_edges(i,j) == 1 and G.number_of_edges(i,k) == 0 and k in G.nodes() and G.number_of_edges(j,k) == 0 and Voisin(k,V,G):
                                V.append(k)
                    """
                    if len(V) > 3:
                        X.append(V)
            return X
    else:
            return None

def Voisin(c,G):
    k = []
    for i in range(len(c)):
            for j in range(len(c)):
                if j > i and G.number_of_edges(c[j],c[i]) == 0:
                    return False
    return True
if __name__ == "__main__":
    M = []
    fh = open("C:/Users/LENOVO/Desktop/karate.edgelist", "rb")
    G = nx.read_edgelist(fh, nodetype=int)
    node_nums = G.number_of_nodes()
    y = node_nums % 100
    if y != 0:
        for i in range(100 - y):
            G.add_node(node_nums + i)
    node_num = G.number_of_nodes()

    bipartie = []

    T = nx.edges(G)
    T = np.asarray(T)
    O = nx.Graph()
    O.add_edges_from(T, nodetype=int)
    start = time.time()
    print("====== Bipartite Start =======")
    with Pool() as p:
        x = p.starmap(localisation, zip(repeat(O), sorted(O.nodes())))
        bipartie.append(x)
    end = time.time()
    print("======= End of first step =======")
    print("Time", end - start)

    flat_list = [item for sublist in bipartie for item in sublist if item]
    #print("flat 1",len(flat_list)) #,flat_list)
    flat_list = [item for sublist in flat_list for item in sublist]
    #print("flat 2",len(flat_list))#,flat_list)
    np.save('graph/test_Bip.npy', flat_list)

    new_k = []
    k_new = []
    for elem in flat_list:
        x = True
        if sorted(elem) not in new_k:
            for elem2 in new_k:
                if set(elem).issubset(set(elem2)) and elem != elem2:
                    x = False
            if x:
                new_k.append(sorted(elem))
                k_new.append(elem)
    bipartie = k_new

    sz = []
    flat_list = [item for sublist in flat_list for item in sublist]
    R = np.unique(flat_list)

    #print("R",np.unique(R),len(np.unique(R)))
    node_bip = np.zeros(300)
    node_bip[np.unique(R)] = 1
    #print(node_bip)
    PatternFinding(G.edges, node_bip)
    Training()
    fo = open("example1.model", "a")
    stri = "bc "
    for i in flat_list:
        x = True
        strt = ' '.join(map(str, i[0:2]))
        stry = ' '.join(map(str, i[-(len(i) - 2):]))
        fo.write(stri + strt + ',' + stry + "\n")
    fo.close()
    fh = open("C:/Users/LENOVO/Desktop/karate.edgelist", "rb")
    G = nx.read_edgelist(fh)
    T = nx.edges(G)
    T = np.asarray(T)
    E = T
    data = []
    for a, b in G.edges():
        data.append([a, b])
    data = np.asarray(data)
    data = np.expand_dims(data, axis=0)

    for i in range(len(E)):
        x = E[i, 0]
        c = E[i, 1]
        if (int(x) not in R or int(c) not in R):
            G.remove_edge(x,c)
    T = nx.edges(G)
    T = np.asarray(T)
    end = time.time()
    print("======= End of seconde step =======")
    print("Time", end - start)
    sz.append(T)
    np.save('graph/test.npy', data)
    np.save('graph/sam.npy', sz[0])
    np.save('graph/labfin.npy', node_bip)


