import numpy as np

def Filewrite(E,fo):
    for i in E:
        if len(i) > 3:
            stri = "st "
            print("i ",i)
            stri = stri + str(i[0]) + ", "
            for j in i:
                if j != i[0]:
                    stri = stri + str(j) + " "
            strt = stri[-3:]
            if ',' not in strt:
                fo.write(stri + "\n")

def Filewrite2(E,fo):
    stri = "fc "
    R = []
    for i in E:
        x = True
        for j in E:
            if set(i).issubset(set(j)) and i != j or len(i) < 4:
                x = False
        if x:
            R.append(set(i))
            strt = ' '.join(map(str, i))
            fo.write(stri + strt + "\n")

L = np.load("graph/test_Star.npy", allow_pickle=True)
C = np.load("graph/test_Cliques.npy", allow_pickle=True)

print(L)
T = []
for i in L:
    size = len(i)
    for j in C:
        supp = []
        if len(i) > 0 and str(i[0]) in j:
            for z in range(len(i)):
                if i[z] != i[0] and str(i[z]) in j:
                    supp.append(i[z])
            if len(supp) > 0 and j not in T:
                T.append(j)
                for k in supp:
                    i.remove(k)
print(L)
print(T,len(T),len(C))
fo = open("example1.model", "w")
Filewrite(L,fo)
Filewrite2(T,fo)
fo.close()
np.save('graph/test_Cliques.npy', T)
