
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import time

def Recuperation(G):
    #recuperation de tous les noeuds du graph avec une taille > x.
    T = []
    for i in G.nodes():
        if len(G.edges(i)) >= 2:
            T.append(i)
    Suppression(T,G)
def Suppression(T,G):
    #retirer les liens avec les noeuds non recupérer
    E = nx.edges(G)
    E = np.asarray(E)
    M = []
    for i in range(len(E)):
        if E[i,0] not in T or E[i,1] not in T:
            M.append(i)

    size = len(E)
    E = [v for i,v in enumerate(E) if i not in M]
    E = np.array(E)
    new_size = len(E)
    if size != new_size:
        O = nx.Graph()
        O.add_edges_from(E, nodetype=int)
        Recuperation(O)
    else:
        O = nx.Graph()
        O.add_edges_from(E, nodetype=int)

def Filewrite(E):
    stri = "fc "
    fo = open("example1.model", "a")
    R = []
    for i in E:
        x = True
        for j in E:
            if set(i).issubset(set(j)) and i != j:
                x = False
        if x:
            R.append(set(i))
            strt = ' '.join(map(str, i))
            fo.write(stri + strt + "\n")
    fo.close()
    print("The file contains: ")
    fo = open("example1.model", "r")
    dummy = fo.read()
    print(dummy)
    fo.close()
    import os
    print(os.path.abspath("example1.model"))

fh = open("C:/Users/LENOVO/Desktop/karate.edgelist", "rb")
G = nx.read_edgelist(fh,nodetype=int)
x = G.number_of_nodes()
print(nx.number_of_nodes(G))
y = x % 100
if y != 0:
    for i in range(100-y):
        G.add_node(x+i)
start = time.time()
x = list(nx.find_cliques(G))
Filewrite(x)
print(time.time()-start)
start = time.time()
Recuperation(G)
Filewrite(x)
print(time.time()-start)

