from multiprocessing import Pool
from itertools import repeat
import numpy as np
import copy
from compact2 import Compact2
def Filewrite(E):
    fo = open("example1.model", "w")
    for i in E:
        stri = "st "
        print("i ",i)
        stri = stri + str(i[0]) + ", "
        for j in i:
            if j != i[0]:
                stri = stri + str(j) + " "
        strt = stri[-3:]
        if ',' not in strt:
            fo.write(stri + "\n")
    fo.close()
def localisation(L,i):
    for j in L:
            if j != i and i[0] in j:
                i.remove(j[0])
    return i
def Compact(flat_list):
    print("=======Start compact========")

    new_L = []
    L2 = copy.deepcopy(flat_list)
    for i in range(len(flat_list)):
        if len(flat_list[i]) > 2:
            for j in range(len(flat_list)):
                if len(flat_list[j]) > 2:
                    x = flat_list[i]
                    if flat_list[i] != flat_list[j] and x[0] in L2[j]:
                        y = flat_list[j]
                        if y[0] in L2[i]:
                            L2[i].remove(y[0])
            new_L.append(L2[i])
    np.save('graph/test_Star.npy', new_L)
    print("wsel")
    Compact2(new_L)