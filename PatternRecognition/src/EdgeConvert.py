import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import copy
from networkx.algorithms import clique
import time
from ANN_CLIQUES import PatternFinding
from train import Training

def Filewrite(E):
    stri = "fc "
    fo = open("example1.model", "a")
    R = []
    for i in E:
        #x = True
        #for j in E:
        #    if set(i).issubset(set(j)) and i != j:
        #        x = False
        #if x:
            #R.append(set(i))
            strt = ' '.join(map(str, i))
            fo.write(stri + strt + "\n")
    fo.close()
    import os
    #print(os.path.abspath("example1.model"))
#print("lancement")
fh = open("C:/Users/LENOVO/Desktop/karate.edgelist", "rb")
start = time.time()
G = nx.read_edgelist(fh)
x = G.number_of_nodes()
#print(x)
print("====== Start Cliques ======")
y = x % 100
if y != 0:
    for i in range(100-y):
        G.add_node(x+i)

total_size = G.number_of_nodes()
cliques = list(clique.find_cliques(G))
flat_list = [sublist for sublist in cliques if len(sublist) > 5]
cliques = np.asarray(([y for x in flat_list for y in x if len(x) >= 5]))

#print("cliques",flat_list,len(flat_list))
fo = open("example1.model", "w")
#fo.write(str(flat_list))
#fo.close()

node_cliques = np.unique(cliques)
nodes_cliques = node_cliques.astype(int)

val_map = {}
key = []
i = 0
values = [val_map.get(node, 0.25) for node in G.nodes()]

""" pour les quasi-cliques
key=np.zeros(G.number_of_nodes())
for clique in cliques:
    for node in clique:
        val_map[node]=1.0
        key[int(node)] = 1
        i = i+1
key[nodes_cliques]=1
"""
output = np.zeros(G.number_of_nodes())
output[nodes_cliques] = 1
#print(output)
end = time.time()
print("====== End of first step ======")
print("Time",end-start)
M = []
data=[]
for a,b in G.edges():
    data.append([a,b])
data = np.asarray(data)
data = np.expand_dims(data,axis=0)
PatternFinding(G.edges,output)
Training()

Filewrite(flat_list)
np.save('graph/test_Cliques.npy', flat_list)
sz = []

T = nx.edges(G)
E = np.asarray(T)
for i in range(len(E)):
    x = E[i, 0]
    c = E[i, 1]
    if (x not in node_cliques) or (c not in node_cliques):
            G.remove_edge(x, c)
    T = nx.edges(G)
    T = np.asarray(T)
sz.append(T)
#print(sz)
end = time.time()
print("====== End of second step ======")
print("Time",end-start)
np.save('graph/test.npy', data)
np.save('graph/sam.npy', sz[0])
np.save('graph/labfin.npy',output)
