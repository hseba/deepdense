
import random,numpy as np,os

def Voisin(x,k):
    if len(k) > 0:
        for i in k:
            if set(x).issubset(set(i)):
                return False
        return True
    return True

V = np.load("node2vec/src/graph/test_Bip.npy", allow_pickle=True)
V = list(V)
k = []
T = []
fo = open("example1.model", "w")
stri = "bc "
compteur = 0

for i in range(len(V)):
    print(V[i+compteur])
    x = V[i+compteur]
    k = x[:2]
    if Voisin(x,T):
        for j in range(V.index(x)+1 ,len(V)):
            y = V[j]
            compteur = compteur + 1
            if x[2:] == y[2:] and x[0] == y[0]:
                k.append(y[1])
            else:
                break
        if len(k) > 1:
            strt = ' '.join(map(str, k))
            stry = ' '.join(map(str, x[2:]))
            fo.write(stri + strt + ',' + stry + "\n")
