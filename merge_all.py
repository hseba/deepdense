import os
import numpy as np
#merge les data.npy dans un seul npy
BASE_PATH = "data"
DIR ="clique"
P = os.path.join(BASE_PATH, DIR)
PATH = os.path.join(P, "data.npy")
All_Data=list(np.load(PATH,allow_pickle=True))
PATH = os.path.join(P, "labels.npy")
ALL_Labels =list(np.load(PATH,allow_pickle=True))
for i in range(2,7):
    DIR="clique_"+str(i)
    P = os.path.join(BASE_PATH,DIR)
    PATH = os.path.join(P,"data.npy")
    data = np.load(PATH,allow_pickle=True)
    PATH = os.path.join(P, "labels.npy")
    labels = np.load(PATH,allow_pickle=True)
    All_Data.extend(list(data))
    ALL_Labels.extend(list(labels))
    print(data.shape)
All_Data = np.asarray(All_Data)
ALL_Labels = np.asarray(ALL_Labels)
print(All_Data.shape)
print(ALL_Labels.shape)
np.save('data/data.npy',All_Data)
np.save('data/labels.npy',ALL_Labels)