import random
import numpy as np,os
import numba as nb
import time
import networkx as nx

def delta(A,B,U,m):
    for i in nb.prange(len(U)):
        z = np.zeros(len(U))
        x = np.zeros(len(U))
        c = np.asarray([i+1], dtype=np.str)
        if( np.count_nonzero(V == 1) > 0):
            if c[0] in O.nodes():
                for j in range(len(U)):
                    m = np.asarray([i+1 , j+1], dtype=np.str)
                    if V[j] != 0 and (m[1] in O.nodes() and O.number_of_edges(m[0], m[1]) != 0):
                        x[i] = x[i]
                    else:
                        x[i] = x[i] + V[j]
        U[i] = U[i] + (-x[i] + B * h(i,x[i]))
def h(i,r):
    if r + V[i] == 0:
        return 1
    else:
        return 0
def output(X):
    for i in range(len(X)):
        if (X[i] > 0):
            V[i] = 1
        else:
            V[i] = 0

def CHANGE(A):
    N = []
    E = A
    R = O.edges
    x = list(O.nodes())
    for i in range(len(U)):
        if U[i] > 0:
            print("true")
            N.append(i+1)
    if len(N) > 0:
        for k in x:
            for v in x:
                if v in x and k in x and O.number_of_edges(k, v) > 0:
                    O.remove_edge(k, v)
        A = O.edges

    print("new len A",len(A))
    return A
def Remplire(i):
    x = lab[i]
    for i in range(len(U)):
        if U[i] >= 0 and x[i] > 0:
            Ufin[i] = U[i]

def Arrange(x,i):
    t=0
    y=lab[i]
    for i in range(len(x)):
        if y[i] == 1:
                x[i] = B
        else:
                x[i] = random.uniform(-400.5,-0.5)

lab = np.load("data/clique_2/labels.npy",allow_pickle=True)
dat = np.load("data/clique_2/sam.npy",allow_pickle=True)
start = time.time()
outputs = []

for i in range(len(lab)):

    print(dat[i])
    O = nx.Graph()
    O.add_edges_from(dat[i], nodetype=int)
    m = np.array(O.nodes)
    size = O.number_of_nodes()
    print("====== Increasing embedding step =======")
    adj = np.count_nonzero(lab[i] == 1)
    size = len(lab[i])

    Ufin = np.random.uniform(-19,-1,size)*0
    x = 1
    U = np.random.uniform(-19,-1,size)
    V = np.random.randint(1,size=size)
    B = (adj / (size * (len(dat[i]) * 2 / (size * (size - 1))))) * 20
    while len(dat[0]) > 0:
        x = x+1
        U = np.random.uniform(-19,-1,size)
        delta(dat,B,U,m)
        output(U)
        dat[i] = CHANGE(dat[i])
        Remplire(i)

        O = nx.Graph()
        O.add_edges_from(dat[i])
        m = np.array(O.nodes)
    out = np.asarray(Ufin)
    Arrange(Ufin,i)
    output(Ufin)
    outputs.append(out)
    print("les resultats")
    print(np.count_nonzero(Ufin > 0))
    print(np.count_nonzero(V == 1))
    print(np.count_nonzero(lab[i] == 1))
end = time.time()
print("====== End of increasing ======")
print("Time", end-start)

out = np.asarray(outputs)
print(out.shape)
np.save(os.path.join("INoutput_data.npy"), out)  # generation des outputs"""
