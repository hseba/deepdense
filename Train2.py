import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import  Conv1D,MaxPool1D,Flatten,Dense,Activation,BatchNormalization,Dropout,LSTM#,CuDNNLSTM
import numpy as np
from keras.optimizers import SGD
from keras import backend as K
from keras.utils.generic_utils import get_custom_objects
from tensorflow.keras.models import load_model
import sklearn.preprocessing as s
import time
import matplotlib.pyplot as plt

def step(x):
    return K.relu(K.sigmoid(x))
get_custom_objects().update({'step': Activation(step)})

def load_data(DIR="subDataSet/EMB"):
    train_x = np.load("data_train.npy",allow_pickle=True)
    test_x = np.load("data_test.npy",allow_pickle=True)
    val_x = np.load("data_val.npy",allow_pickle=True)
    print(train_x[0].shape)
    train_y = np.load("label_train.npy")
    test_y = np.load("label_test.npy")
    val_y= np.load("label_val.npy")
    print(val_y.shape)

    print(val_x[0])
    print(val_y[0])
    #normalize
    #train_x = (train_x-train_x.min())/(train_x.max()-train_x.min())
    #test_x = (test_x-test_x.min())/(test_x.max()-test_x.min())
    #val_x = (val_x-val_x.min())/(val_x.max()-val_x.min())
    return train_x,train_y,test_x,test_y,val_x,val_y

def model(input_shape):
    model = Sequential()

    model.add(LSTM(1,return_sequences=True,input_shape=input_shape))
    model.add(Flatten())
    model.add(Dense(200))
    model.add(Dense(100,activation="sigmoid"))
    model.summary()
    opt = tf.keras.optimizers.Adamax(learning_rate=0.01)
    model.compile(loss='mse',optimizer="adamax",metrics=['binary_accuracy'])
    return model

train_x,train_y,test_x,test_y,val_x,val_y = load_data()
model=model((100,6))
print(train_x.shape)
print(test_x.shape)
print(val_x.shape)

history = model.fit(train_x,train_y,validation_data=(val_x,val_y),epochs=300,batch_size=8)

model.save('model_new.h5')

model = load_model('model_new.h5')
print(model.evaluate(test_x,test_y))

print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['binary_accuracy'])
plt.plot(history.history['val_binary_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
"""
print( 2900 % 100)
data_train=np.load('INoutput_data_val.npy')
print("data_train",type(data_train),data_train.shape,data_train)
tab = []
start = time.time()
for i in range(2900):
    tab.append(data_train[i])
    if len(tab) == 100:
        tab = np.asarray(tab)
        #print("data ",i,tab.shape)
        tab = np.expand_dims(tab,axis=0)
        pred = model.predict(tab)[0]
        #print("pred", pred[24],tab[0,24])
        pred = np.round(pred)
        result = np.where(pred == 1)
        print("pred 2", result[0]+(i-99),len(result[0]))
        tab = []

end = time.time()
"""
