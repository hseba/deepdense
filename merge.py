import os,numpy as np
BASE_PATH = "data_val"
#DIR ="clique_1"
PATH = os.path.join(BASE_PATH)

#merger les files dans un seule file "data.npy"
def read(file):
    file_path=os.path.join(PATH,file)
    lines = open(file_path).read().splitlines()
    output=[]
    for line in lines:
        output.append([int(x) for x in line.split(' ')])
    output = np.asarray(output)
    print(output)
    return output


data=[]
for _,_,files in os.walk(os.path.join(PATH)):
    for file in files:
        if ".npy" not in file:
            data.append(read(file))
            print(data[-1].shape)
data = np.asarray(data)
np.save(os.path.join(PATH,"data.npy"),data)
#data = np.load(os.path.join(PATH,"data.npy"),allow_pickle=True)
print(data.shape)
