import os
import numpy as np

#combinaison des fichiers obtenus après l'embadding dans un seul tableau numpy pour chaque ensemble de données

DIRS = ['data_test','data_val','data_train']
for DIR in DIRS:
    for _,_,files in os.walk(DIR):
        data = np.zeros((len(files),100,6))
        i = 0
        for file in files:
            t = np.load(os.path.join(DIR,file),allow_pickle=True)
            print(t.shape,i)
            data[i]=t
            i+=1
        np.save(os.path.join(DIR,DIR),data)
        print(data.shape)
    print(DIR,data)