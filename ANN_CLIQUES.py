import random
import numpy as np,os
import numba as nb
import time
import networkx as nx

def delta(A,B,U,m,V,O):
    for i in nb.prange(len(U)):
        z = np.zeros(len(U))
        x = np.zeros(len(U))
        c = np.asarray([i+1], dtype=np.str)
        if( np.count_nonzero(V == 1) > 0):
            if c[0] in O.nodes():
                for j in range(len(U)):
                    m = np.asarray([i+1 , j+1], dtype=np.str)
                    if V[j] != 0 and (m[1] in O.nodes() and O.number_of_edges(m[0], m[1]) != 0):
                        x[i] = x[i]
                    else:
                        x[i] = x[i] + V[j]
        U[i] = U[i] + (-x[i] + B * h(i,x[i],V))
def h(i,r,V):
    if r + V[i] == 0:
        return 1
    else:
        return 0
def output(X,V):
    for i in range(len(X)):
        if (X[i] > 0):
            V[i] = 1
        else:
            V[i] = 0

def CHANGE(A,O,U):
    N = []
    E = A
    R = O.edges
    x = list(O.nodes())
    for i in range(len(U)):
        if U[i] > 0:
            N.append(i+1)

    if len(N) > 0:
        for k in x:
            for v in x:
                if v in x and k in x and O.number_of_edges(k,v) > 0:
                    O.remove_edge(k, v)
        A = O.edges


    return A
def Remplire(U,Ufin,lab):
    for i in range(len(U)):
        if U[i] >= 0 and lab[i] > 0:
            Ufin[i] = U[i]
"""
        else:
            if lab[i] == 0:
                Ufin[i] = random.uniform(-400.5, -0.5)
            else:
                Ufin[i] = random.uniform(0.5, 400.5)
"""
def Arrange(lab, x, B, V):
    t=0
    y=0
    for i in range(len(x)):
        if lab[i] == 1:
                x[i] = B
                V[i] = 1
        else:
                x[i] = -B
                V[i] = 0


def PatternFinding(dat,lab):

    O = nx.Graph(dat)
    m = np.array(O.nodes)
    size = O.number_of_nodes()
    print("====== Increasing embedding step =======")
    adj = np.count_nonzero(lab == 1)
    size = len(lab)
    for i in range(1):
        Ufin = np.random.uniform(-1, 0, size) * 0
        #print("ufin",Ufin)
        #print(len(dat) * 2 / ((size-1) * (size - 1)))
        x = 1
        U = np.random.uniform(-1, 0, size)
        V = np.random.randint(1, size=size)
        B = (adj / (size * (len(list(O.edges)) * 2 / (size * (size - 1)))))
        #print("B",B)
        Arrange(lab,Ufin,B,V)
        #print(np.count_nonzero(V == 1))
        #print(np.count_nonzero(lab == 1))
        """
        while len(dat) > 0:
            x = x + 1
            U = np.random.uniform(-19, -1, size)
            delta(dat, B, U, m, V, O)
            output(U, V)
            # print(np.count_nonzero(U >= 0))
            # print(np.count_nonzero(lab == 1))
            dat = CHANGE(dat, O, U)
            print("hna")
            Remplire(U,Ufin,lab)
            # print("size",np.count_nonzero(Ufin >= 0),np.count_nonzero(U >= 0))
            # print(len(dat))
            O = nx.Graph(dat)
            #O.add_edges_from(dat)
            m = np.array(O.nodes)
        out = np.asarray(Ufin)
        Arrange(lab, Ufin, B)
        output(Ufin, V)
        outputs.append(out)
        print(np.count_nonzero(Ufin > 0))
        print(np.count_nonzero(V == 1))
        print(np.count_nonzero(lab == 1))1
        """
    #end = time.time()
    #print("====== End of increasing ======")
    #print("Time", end - start)
    out = np.asarray(Ufin)

    #out = np.asarray(outputs)
    # print(outputs)
    # print(lab)
    np.save(os.path.join("INoutput_data_val.npy"), out)  # generation des outputs"""

#lab = np.load("node2vec/src/graph/labfin.npy",allow_pickle=True)
#dat = np.load("node2vec/src/graph/sam.npy",allow_pickle=True)
#print(lab)
#print(type(dat))
#PatternFinding(dat,lab)