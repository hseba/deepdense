from networkx.generators import community
from networkx.generators import random_graphs
from networkx.algorithms import clique
import networkx as nx
import random,numpy as np,os
def generate_clique(nb,size,total_size):
    sub=community.caveman_graph(nb,size)#Generation de de graphes en forme de cliques
    G=random_graphs.fast_gnp_random_graph(total_size,0.05)#Generation de graphes aleatoires avec 0.1% de liens
    G=nx.compose(G,sub) #fusion des deux graphes, obtention d'un graphe aleatoire avec nb cliques
    node_mapping = dict(zip(G.nodes(), sorted(G.nodes(), key=lambda k: random.random())))#creation du mapping
    G_new = nx.relabel_nodes(G, node_mapping)#application du mapping
    cliques=list(clique.find_cliques(G_new))
    cliques=np.asarray(([y for x in cliques for y in x  if len(x)>=4]))
    nodes_cliques = np.unique(cliques)
    print(len(nodes_cliques))
    x = len(nodes_cliques)
    output=np.zeros(total_size)
    output[nodes_cliques]=1
    return G_new,output,x,G_new.edges()

def generate_without_clique(total_size):#generation de graphes aleatoires sans cliques
    while True:
        G=random_graphs.fast_gnp_random_graph(total_size,0.04)
        cliques=list(clique.find_cliques(G))
        cliques=[x for x in cliques if len(x)>=4]
        if len(cliques)==0:
            break
    return G, np.zeros(total_size)


def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="clique_2"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_size_clique = 10
max_clique_count = 10
outputs = []
Edgelist = []
Gr_size = 1000
data = []
x = 0
for id in range(Gr_size):
    G,labels,y,edgelist = generate_clique(random.randint(4,max_clique_count),random.randint(4,max_size_clique),total_size)
    x = x + y
    outputs.append(labels)
    tab = to_input_shape(G)
    data.append(tab)
    Edgelist.append(edgelist)
np.save(os.path.join(PATH, "data.npy"), np.asarray(data))

output = np.asarray(outputs)
Edgelist = np.asarray(Edgelist)
np.save(os.path.join(PATH,"labels.npy"),output) #generation des outputs
np.save(os.path.join(PATH,"sam.npy"),Edgelist) #generation des outputs