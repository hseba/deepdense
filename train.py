import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import  Conv1D,MaxPool1D,Flatten,Dense,Activation,BatchNormalization,Dropout,LSTM#,CuDNNLSTM
import numpy as np
from keras.optimizers import SGD
from keras import backend as K
from keras.utils.generic_utils import get_custom_objects
from tensorflow.keras.models import load_model
import sklearn.preprocessing as s
import time

def load_data(DIR="subDataSet/EMB"):
    train_x = np.load("node2vec/src/graph/data_train.npy",allow_pickle=True)
    test_x = np.load("node2vec/src/graph/data_test.npy",allow_pickle=True)
    val_x = np.load("node2vec/src/graph/data_val.npy",allow_pickle=True)
    print(train_x[0].shape)
    train_y = np.load("node2vec/src/graph/label_train.npy")
    test_y = np.load("node2vec/src/graph/label_test.npy")
    val_y= np.load("node2vec/src/graph/label_val.npy")
    print(val_x.shape)
    print(val_y.shape)
    print(train_x.shape)
    print(train_y.shape)
    print(test_x.shape)
    print(test_y.shape)
    print(val_y[0])
    print("normalize",val_x[0])

    return train_x,train_y,test_x,test_y,val_x,val_y

def model(input_shape):
    model = Sequential()
    #model.add(Conv1D(80,3,input_shape=(1,100)))
    #model.add(MaxPool1D(9))
    #model.add(Conv1D(10,3))
    #model.add(MaxPool1D(9))
    #model.add(LSTM(1,return_sequences=True,input_shape=input_shape))
    #model.add(LSTM(80))

    #model.add(Flatten())
    #model.add(Dropout(0.3))
    model.add(Dense(200,input_shape=(100,)))
    model.add(Dense(100,activation="sigmoid"))
    model.summary()
    opt = tf.keras.optimizers.Adamax(learning_rate=0.01)
    model.compile(loss='mse',optimizer="adamax",metrics=['binary_accuracy'])
    return model


#train_x,train_y,test_x,test_y,val_x,val_y = load_data()
#print("shape",train_x.shape)
#train_x = train_x.reshape(-1,100,)
#test_x  = test_x.reshape(-1,100,)
#train_y = train_y.reshape(-1,100,)
#test_y = test_y.reshape(-1,100,)
#val_x  = val_x.reshape(-1,100,)
#val_y = val_y.reshape(-1,100,)

def Training():
    #model = model((100,))

    #model.fit(train_x,train_y,validation_data=(val_x,val_y),epochs=800,batch_size=8)
    print("===================Start of Finding Patterns======================")
    #model.save('Final_model.h5')
    start = time.time()

    model = load_model('Final_model.h5')
    #print(model.evaluate(test_x, test_y))
    #print(test_x.shape, test_x[0])
    # data_train = np.load('INoutput_data_val.npy')
    # data_tr = data_train.reshape(-1, 100)
    # print(data_tr.shape)
    #pred = model.predict(test_x)
    #pred = np.round(pred)
    #print(pred[0], len(pred), len(pred[0]))
    # print( 900 % 100)
    data_train = np.load('INoutput_data_val.npy')
    print("data",data_train[1])
    #data_train = np.append(data_train, -500)
    #print(data_train, data_train.shape)
    data_tr = data_train.reshape(-1, 100)
    #print(data_tr[28])
    #print(data_tr.shape)
    pred = model.predict(data_tr)
    pred = np.round(pred)
    print("pred",pred)
    #for i in range(len(pred)):
    #    print(pred[i], np.count_nonzero(pred[i]))
    # print("data_train", type(data_tr), data_train.shape, data_tr[0].shape)  # ,data_train,data_train[1])
    """
    tab = []
    start = time.time()
    for i in range(len(data_tr)):
        # tab.append(data_tr[i])
        # print(data_tr[i].shape)
        pred = model.predict(data_tr[i])[0]
        pred = np.round(pred)
        if pred == 1:
            print("pred : ", i, pred)
        # print(len(tab))
        
        if len(tab) == 100:
            tab = np.asarray(tab)
            #print("data ",i,tab.shape)
            tab = np.expand_dims(tab,axis=0)
            pred = model.predict(tab)[0]
            #print("pred", pred[24],tab[0,24])
            pred = np.round(pred)
            result = np.where(pred == 1)
            print("pred 2", result[0]+(i-99),len(result[0]))
            tab = []
        """
    end = time.time()
    print("===================End of Finding Patterns======================")

    print("Time", end - start)


