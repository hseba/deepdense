
import numpy as np
import time
from sklearn.linear_model import LogisticRegression
import classify
import deepwalk
import matplotlib.pyplot as plt
import networkx as nx
from sklearn.manifold import TSNE
from gensim.models import Word2Vec
from multiprocessing import Pool
from itertools import repeat

def evaluate_embeddings(embeddings):
    X, Y = classify.read_node_label('../data/wiki/wiki_labels.txt')
    tr_frac = 0.8
    print("Training classifier using {:.2f}% nodes...".format(
        tr_frac * 100))
    clf = classify.Classifier(embeddings=embeddings, clf=LogisticRegression())
    clf.split_train_evaluate(X, Y, tr_frac)

def read(arr):
    G = nx.Graph()
    for a in arr:
        print(a[0])
        if not G.has_node(a[0]):
            G.add_node(a[0])
        if not G.has_node(a[1]):
            G.add_node(a[1])
        G.add_edge(a[0],a[1],weight=1)
    return G

def plot_embeddings(embeddings,):
    X, Y = classify.read_node_label('../data/wiki/wiki_labels.txt')

    emb_list = []
    for k in X:
        emb_list.append(embeddings[k])
    emb_list = np.array(emb_list)
    print(emb_list.shape)

    model = TSNE(n_components=2)
    node_pos = model.fit_transform(emb_list)

    color_idx = {}
    for i in range(len(X)):
        color_idx.setdefault(Y[i][0], [])
        color_idx[Y[i][0]].append(i)

    for c, idx in color_idx.items():
        plt.scatter(node_pos[idx, 0], node_pos[idx, 1], label=c)
    plt.legend()
    plt.show()
def Affect(z,i):
    #for i in z.wv.index2entity:
        # key = i + 1
        # if key in z.wv.index2entity:
        print(i)
        print(z.index2entity.index(i))
        # print(z.wv.vectors[z.wv.index2entity.index(key)])
        x = z.index2entity.index(i)
        return z.vectors[x]
def read_all():
    data = np.load('graph/test_Bip.npy',allow_pickle=True)
    id=0
    results = [] #a retirer
    print(len(data),len(data[0]),data[0])
    for x in data:
        w = time.time()
        G=read(x)
        #print("A",len(G))
        model = deepwalk.DeepWalk(G, walk_length=5, num_walks=4, workers=1)
        z = model.train(window_size=5, iter=3)
        print(len(z.wv.vectors))
        result = np.zeros((116835, int(6)))

        for i in z.wv.index2entity:
                x = z.wv.index2entity.index(i)
                result[i] = z.wv.vectors[x]
        results.append(result)
        name = str('graph/data_train.npy')
        name = name[:name.index('.')]
        np.save(name+"\\transformed_"+str(id),results)
        print(id, "DONE")
        id += 1
    np.save('graph/test_train.npy', results)
    return model
if __name__ == "__main__":
    '''G = nx.read_edgelist('../data/wiki/Wiki_edgelist.txt',
                         create_using=nx.DiGraph(), nodetype=None, data=[('weight', int)])

    model = deepwalk.DeepWalk(G, walk_length=15, num_walks=80, workers=1)
    z = model.train(window_size=5, iter=3)
    print(len(z.wv.vectors))
    result = np.zeros((2405, int(64)))
    for i in range(len(z.wv.vectors)):
        key = str(i + 1)
        if key in z.wv.index2entity:
            result[i] = z.wv.vectors[z.wv.index2entity.index(key)]
    result = np.asarray(result)
    print("result shape",result.shape)'''
    model = read_all()
    #embeddings = model.get_embeddings()
    #print(embeddings)
    #evaluate_embeddings(embeddings)
    #plot_embeddings(embeddings)
