
import numpy as np
import classify
from sklearn.linear_model import LogisticRegression
import line
import matplotlib.pyplot as plt
import networkx as nx
from sklearn.manifold import TSNE


def evaluate_embeddings(embeddings):
    X, Y = classify.read_node_label('../data/wiki/wiki_labels.txt')
    tr_frac = 0.8
    print("Training classifier using {:.2f}% nodes...".format(
        tr_frac * 100))
    clf = classify.Classifier(embeddings=embeddings, clf=LogisticRegression())
    clf.split_train_evaluate(X, Y, tr_frac)


def plot_embeddings(embeddings,):
    X, Y = classify.read_node_label('../data/wiki/wiki_labels.txt')

    emb_list = []
    for k in X:
        emb_list.append(embeddings[k])
    emb_list = np.array(emb_list)

    model = TSNE(n_components=2)
    node_pos = model.fit_transform(emb_list)

    color_idx = {}
    for i in range(len(X)):
        color_idx.setdefault(Y[i][0], [])
        color_idx[Y[i][0]].append(i)

    for c, idx in color_idx.items():
        plt.scatter(node_pos[idx, 0], node_pos[idx, 1], label=c)
    plt.legend()
    plt.show()

def read(arr):
	G = nx.Graph()
	for a,b in arr:
		if not G.has_node(a):
			G.add_node(a)
		if not G.has_node(b):
			G.add_node(b)
		G.add_edge(a,b,weight=1)
	return G

def read_all():
    data = np.load('graph/data_val.npy',allow_pickle=True)
    id=0
    for x in data:
        G=read(x)
        model = line.LINE(G, embedding_size=64, order='second')
        model.train(batch_size=120, epochs=100, verbose=2)
        embeddings, result = model.get_embeddings()
        result = np.asarray(result)
        result = np.asarray(result)
        name = str('graph/data_val.npy')
        name = name[:name.index('.')]
        np.save(name+"\\transformed_"+str(id),result)
        print(id,"DONE")
        id+=1
    return model
if __name__ == "__main__":
    '''G = nx.read_edgelist('../data/wiki/Wiki_edgelist.txt',
                         create_using=nx.DiGraph(), nodetype=None, data=[('weight', int)])

    model = line.LINE(G, embedding_size=64, order='second')
    model.train(batch_size=1024, epochs=50, verbose=2)
    print("hnaya",model)
    embeddings, result = model.get_embeddings()
    #print(embeddings)
    result = np.asarray(embeddings)
    #print(result)
    #evaluate_embeddings(embeddings)
    #plot_embeddings(embeddings)'''
    model = read_all()
