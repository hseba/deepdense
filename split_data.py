import numpy as np

#chargement des données
#data  = np.load('data/clique_1/data.npy',allow_pickle=True)
#matrix  = np.load('data/clique_1/data2.npy',allow_pickle=True)
#out  = np.load('data/clique_1/output.npy',allow_pickle=True)
size  = np.load('data/clique_1/size.npy',allow_pickle=True)
labels = np.load('data/clique_1/labels2.npy',allow_pickle=True)
#data = np.load('INoutput_data.npy')

#print(data.shape)
print(labels.shape)
#print(matrix.shape)
#print(out.shape)
print(size.shape)

#print(data[2])
print(labels[0])
#print(matrix[0])
#print(out[0])
print(size[0])

print("DONE")

#définition du ratio train / test
split_rate=0.8
k = int(len(size)*split_rate)

#données d'apprentissage
#data_train = data[:k]
#matrix_train = matrix[:k]
#out_train = out[:k]
size_train = size[:k]
label_train = labels[:k]

#données de test
#data_test = data[k:]
#matrix_test = matrix[k:]
size_test = size[k:]
#out_test = out[k:]
label_test = labels[k:]


#définition du ratio train / validation
split_rate=0.8
k = int(len(size_train)*split_rate)

#données de validation
#data_val = data_train[k:]
#matrix_val = matrix_train[k:]
#out_val = out_train[k:]
size_val = size_train[k:]
label_val = label_train[k:]

#donnée d'apprentissage
#data_train = data_train[:k]
#matrix_train = matrix_train[:k]
#out_train = out_train[:k]
size_train = size_train[:k]
label_train = label_train[:k]

print(label_train.shape,label_test.shape,label_val.shape)
#print(data_train.shape,data_test.shape,data_val.shape)
print(label_train[0])
#print(data_train[0])
#print(out_train.shape,out_test.shape,out_val.shape)
print(size_train.shape,size_test.shape,size_val.shape)
#sauvegarde
#np.save("node2vec/src/graph/data_train.npy", data_train)
#np.save("node2vec/src/graph/data_test.npy", data_test)
#np.save("node2vec/src/graph/data_val.npy", data_val)
#np.save("matrix_train.npy", matrix_train)
#np.save("matrix_test.npy", matrix_test)
#np.save("matrix_val.npy", matrix_val)
#np.save("out_train.npy", out_train)
#np.save("out_test.npy", out_test)
#np.save("out_val.npy", out_val)
np.save("node2vec/src/graph/size_train.npy", size_train)
np.save("node2vec/src/graph/size_test.npy", size_test)
np.save("node2vec/src/graph/size_val.npy", size_val)
np.save("node2vec/src/graph/label_val.npy",label_val)
np.save("node2vec/src/graph/label_train.npy",label_train)
np.save("node2vec/src/graph/label_test.npy",label_test)