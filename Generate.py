from networkx.generators import community
from networkx.generators import random_graphs
from networkx.algorithms import clique
import networkx as nx
import random,numpy as np,os
import copy
import matplotlib.pyplot as plt

def generate_clique(nb,size,total_size):
    sub=community.caveman_graph(nb,size)#Generation de de graphes en forme de cliques
    G=random_graphs.fast_gnp_random_graph(total_size,0.1)#Generation de graphes aleatoires avec 0.1% de liens
    G=nx.compose(G,sub) #fusion des deux graphes, obtention d'un graphe aleatoire avec nb cliques
    node_mapping = dict(zip(G.nodes(), sorted(G.nodes(), key=lambda k: random.random())))#creation du mapping
    G_new = nx.relabel_nodes(G, node_mapping)#application du mapping
    cliques=list(clique.find_cliques(G_new))
    cliques=np.asarray(([y for x in cliques for y in x  if len(x)>=4]))
    nodes_cliques = np.unique(cliques)
    x = len(nodes_cliques)
    #print("nodes_cliques",x)
    output=np.zeros(total_size)
    output[nodes_cliques]=1
    return G_new,output,x,nodes_cliques,size,nb

def generate_without_clique(total_size):#generation de graphes aleatoires sans cliques
    while True:
        G=random_graphs.fast_gnp_random_graph(total_size,0.04)
        cliques=list(clique.find_cliques(G))
        cliques=[x for x in cliques if len(x)>=6]
        if len(cliques)==0:
            break
    return G, np.zeros(total_size)


def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="clique_1"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_size_clique = 10
max_clique_count = 10
outputs = []
Gr_size = 1000
graph = []
data = []
lab = []
nodes = []
input = []
sz = []
B = [None]*total_size
x = 0
for id in range(Gr_size):
    G,labels,y,z,s,ng = generate_clique(random.randint(4,max_clique_count),random.randint(4,max_size_clique),total_size)

    tab = to_input_shape(G)
    graph.append(tab)
    A = nx.adjacency_matrix(G, nodelist=range(total_size), weight='weight')
    A.setdiag(A.diagonal() * 2)
    A = A.todense()
    B = copy.deepcopy(A)
    for i in range(len(B)):
        if i not in z:
            B[i] = 0
    outputs.append(y)
    lab.append(labels)
    data.append(B)
    T = nx.edges(G)
    T = np.asarray(T)
    E = T
    for i in range(len(E)):
        x = E[i,0]
        c = E[i,1]
        if (x not in z) and (c not in z):
            w = -1
            t = np.argwhere(T == (x, y))
            d = np.argwhere(T == (c, x))
            t = np.concatenate((t, d))

            for r in range(len(t)):
                for k in range(len(t)):
                    if (t[r, 0] == t[k, 0]) and r != k and w != t[r, 0]:
                        w = t[r, 0]
                        #print(w)
            P = np.delete(T,w,axis=0)
            T=P
    print("id",id)
    sz.append(T)


np.save(os.path.join(PATH, "size.npy"), np.asarray(sz)) ###########################
#np.save(os.path.join(PATH, "data.npy"), np.asarray(graph)) ############################
#np.save(os.path.join(PATH, "data2.npy"), np.asarray(data)) ##########################
#print("out",sz[0])
#print("out",graph[0])
#print("out",data[0])

output = np.asarray(outputs)
#np.save(os.path.join(PATH,"output.npy"),output) #generation des outputs #######################
#print("out",output[0])
labs = np.asarray(lab)
np.save(os.path.join(PATH,"labels2.npy"),labs) #generation des outputs ##########################
#print("labs",labs[0])
#print(s)
print(len(sz[0]))

#nx.draw(G,with_labels=True)
#plt.show()