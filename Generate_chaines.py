import networkx as nx
import matplotlib.pyplot as plt
from networkx.generators import random_graphs
import random,numpy as np,os
import copy

def find_all_paths(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths
def Chaines():
    G = random_graphs.fast_gnp_random_graph(100, 0.01)
    nx.draw(G,with_labels=True)
    plt.show()

    chaine = nx.chain_decomposition(G,1)
    y = []
    for i in range(100):
        print("hnaya")
        for j in range(100):
            if i != j:
                x = find_all_paths( G, i, j)
                if(len(x) > 0):
                    y.append(x)
    tab = []
    R = []
    for i in range(len(y)):
        if len(y[i]) > 1:
            x = y[i]
            for j in range(len(y[i])):
                for z in range(len(y[i])):
                    if set(x[j]).issubset(set(x[z])) and len(x[z]) > len(x[j]):
                        tab.append(j)
                    else:
                        if set(x[z]).issubset(set(x[j])) and len(x[z]) < len(x[j]):
                            tab.append(z)
            for k in range(len(x)):
                if k not in tab:
                    R.append(x[k])
            tab = []
    print(R)
    return G,R
def generate_clique(nb,size,total_size):
    Chaine = []
    G,ch = Chaines()
    A = nx.adjacency_matrix(G,nodelist=sorted(G.nodes()),weight='weight')
    A.setdiag(A.diagonal() * 2)
    A = A.todense()
    for i in range(len(ch)):
        x = ch[i]
        for j in range(len(x)):
            if x[j] not in Chaine:
                Chaine.append(x[j])
    print("hay la chaine",Chaine)
    output=np.zeros(total_size)
    output[Chaine]=1
    return G,output,len(Chaine),Chaine,(size+nb),A

def to_input_shape(G):# remplissage du fichier .edgelist format noeud -> noeud-voisin
    tab=[]
    for a,b in G.edges():
        tab.append([a,b])
    return tab

BASE_PATH = "data"
DIR ="Bipartie"

if(not os.path.exists(BASE_PATH)):
    os.mkdir(BASE_PATH)
PATH = os.path.join(BASE_PATH,DIR)

if(not os.path.exists(PATH)):
    os.mkdir(PATH)

total_size = 100
max_size_clique = 30
max_clique_count = 30
outputs = []
Gr_size = 1000
graph = []
data = []
lab = []
nodes = []
input = []
sz = []
B = [None]*total_size
x = 0
for id in range(1):
    Bipartie = []
    G,labels,y,z,s,A = generate_clique(random.randint(5,max_clique_count),random.randint(5,max_size_clique),total_size)
    tab = to_input_shape(G)
    graph.append(tab)
    B = copy.deepcopy(A)
    input.append(A)
    for i in range(len(B)):
        if i not in z:
            B[i] = 0
    outputs.append(y)
    lab.append(labels)
    data.append(B)
    sz.append(s)
    print(id)

output = np.asarray(outputs)
labs = np.asarray(lab)
node = np.asarray(input)
print("sz",sz[0])
print("graphe",graph[0])
print("matrix",data[0])
print("out",output[0])
print("labs",labs[0])
print("nodes",node[0])

"""
np.save(os.path.join(PATH, "size.npy"), np.asarray(sz)) ###########################
np.save(os.path.join(PATH, "data.npy"), np.asarray(graph)) ############################
np.save(os.path.join(PATH, "data2.npy"), np.asarray(data)) ##########################
np.save(os.path.join(PATH,"output.npy"),output) #generation des outputs #######################
np.save(os.path.join(PATH,"labels2.npy"),labs) #generation des outputs ##########################
np.save(os.path.join(PATH,"nodes.npy"),node) #generation des outputs
"""
